// alert("Hello, Batch 241")

// ARITHMETIC OPERATORS
let x = 1397;
let y = 7831;

// Addition Operator
let sum = x + y;
console.log("Result of addition operator: " + sum);

// Subtraction Operator
let difference = x - y;
console.log("Result of the subtraction operator: " + difference);

// Multiplication Operator
let product = x * y;
console.log("Result of the multiplication operator: " + product);

// Division Operator
let quotient = x / y
console.log("Result of the division operator: " + quotient);

// Modulo Operator - used if a value is odd or even; or divisible by
// If addition = result must be 0
let remainder = y % x;
console.log("Result of modulo operator: " + remainder);

// ASSIGNMENT OPERATOR
// Basic Assignment Operator (=)
// The assignment operator adds the value of the right operand to a variable and assigns result tot he variable

let assignmentNumber = 8;

// Addition Assignment Operator
// Uses the current value of the variable and it ADDS anumber (2) to itself. Afterwards, it reassigns it as a new value.
// shorthand of this operation: assignmentNumber = assignmentNumber + 2
assignmentNumber += 2 // 8 + 2 = 10
console.log("Result of the addition assignment operator: " + assignmentNumber); //10

// Subtraction/Multiplication/Division (-=, *=, /=)

// Subtraction Assignment Operator
assignmentNumber -= 2; // assignmentNumber = assignmentNumber - 2; 10 - 2 = 8
console.log("Result of the subtaction assignment operator :" + assignmentNumber);

// Multiplication Assignment Operator
assignmentNumber *= 2; //8 * 2 = 16
console.log("Result of the multiplication assignment operator: " + assignmentNumber); //16

//  Division Assignment Operator
assignmentNumber /= 2; // 16 / 2 = 8
console.log("Result of the division assignment operator: " + assignmentNumber); //8


// Multiple Operators and Parentheses
/*
	When multiple operators are applied in a single statement, it follows the PEMDAS Rule (Parenthesis, Exponents, Multiplication, Division, Addition, Subtraction)

	1. 3 * 4 = 12
	2. 12 / 5 = 2.4
	3. 1 + 2 = 3
	4. 3 - 2.4  = 0.6

	1. 4 / 5 = 0.8
	2. 2 / 3 = 0.67
	3. 0.8 * 0.67 = 0.536
	4. 1 + 0.536 = 


*/

 let mdas = 1 + 2 - 3 * 4 / 5
 console.log("Result of mdas operation: " + mdas); //0.6000000000000001

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas operation: " + pemdas); //0.19999999999999996

let sample = 5 % 2 + 10;
/*
	5 % 2 = 1 // 5/2 = 2 remainder 1
	1 + 10 = 11
*/
console.log(sample); // 11

sample = 5 + 2 % 10;
console.log(sample); // 7

/*
	2 % 10 = 0 remainder 2
	5  + 2 = 7
*/

// INCREMENT AND DECREMENT
// Operators that add or subtract values by 1 and reassigns the value of the variable where the increment or decrement was applied to

let z = 1;

// Pre-Increment
// The value of "z" is added by a value of 1 before returning the value and storing it in the variable "increment"
// two types ang increment = pre at post; pag pre = nagbabago din value ng variable na inincrement natin
// let increment = ++z;
// console.log("Result of the preincrement: " + preIncrement); // 2

// console.log("Result of the pre-increment: " + z);

// Post-Increment
// The value of "z" is returned and stored in the variable "postIncrement" then the value of "z" is increased by one
let postIncrement = z++;
console.log("Result of the post-increment: " + postIncrement); // 1
console.log("Result of the postIncrement: " + z); // 2

/*
	pag pre = nagbabago din value ng variable na inincrement natin

	Pre-increment - add 1 first before reading value
	Post-increment - reads the value first before adding 1
*/

let a = 2;

// Pre-Decrement
// The value of "a" is decreased by a value of 1 before returning the value and storing it in the variaable "preDecrement"
let preDecrement = --a;
console.log("Result of the pre-decrement: " + preDecrement); //1
console.log("Result of the pre-decrement" + a); //1

// Post-Decrement 
// The value "a" is returned and stored int he variable "postDecrement" then the value of "a" is decreased by 1;
let postDecrement = a--;
console.log("Result of the postDecrement: " + postDecrement);// 2
console.log("Result of the postDecrement" + a); // 2-1 = 1


// TYPE COERCION
/*
	Type coercion is the automatic or implicit conversion of values from one data type to another


*/

let numA = '10'; //String
let numB = 12; //Number

/*
	Adding/concatenating a string and a number will result as a string

	string > number
*/

let coercion = numA + numB;
console.log(coercion); //1012
console.log(typeof coercion); //String

let coercion1 = numA - numB; //forced coercion; si js na ang nagdecision; js is not always correct, naghanap lang sya ng paraan para isolve yung condition, disadvantage of loosely type programming language like JS
console.log(coercion1); //-2
console.log(typeof coercion1); //Number


// Non-coercion
let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion); //30
console.log(typeof nonCoercion); //number

// Addition of Number and Booelan
/*
	The result is a number
	The boolean "true" is associated with the value of 1

	The boolean "false" is associated with the value of 0
*/



let numE = true + 1; // true = 1; 1 + 1
console.log(numE); ;//2 
console.log(typeof numE); // number

let numF = false + 1; // false = 0; 0 + 1
console.log(numF); // 1
console.log(typeof numF); //number

// COMPARISON OPERATOR
let juan = 'juan';

// Equality Operator(==)
/* 
	- Checks whether the operands are equal/have the same content/value (not data type)
	- Attempts to CONVERT AND COMPARE operands of different data types
	- Returns a boolean value (true/false)
*/

console.log(1 == 1); //1 = 1; true 
console.log(1 == 2); //1 /= 1; false
console.log(1 == '1'); //same value with diff data type; true
console.log(1 == true); //true; true = 1; false = 0

//compare two strings that are the same
console.log('juan' == 'juan'); //True
console.log('true' == true); //False; 'True' cannot be converted, true can be converted to 1
console.log(juan == 'juan')// True ; same content of juan, case sensitive


// Inequality Operator
/*
	- Checks whether the operands are not equal/have different content
	- Attempts to CONVERT AND COMPARE operands of diff data types
*/

console.log(1 != 1); //False
console.log(1 != 2); //True
console.log(1 != '1')

// Strict Equality Operator (===)
/*
	Checks whether the operands are equal/have the same content/value
	- Also COMPARES the data types of 2 values
*/

console.log(1 === 1); //true ; same value and content
console.log(1 === 2); // false; diff content
console.log(1 === '1'); //false; strict na si js na iba ang number sa string
console.log(1 === true); //false;  strict na si js na iba ang number sa boolean
console.log('juan' === 'juan'); //true
console.log(juan === 'juan'); //tue

// Strict Inequality Operator (!==)
/*
	Checks whether the operands are not equal/have the same content and also COMPARES the data types of 2 values
*/

console.log(1 !== 1); //false
console.log(1 !== 2); //true
console.log(1 !== '1'); //true; altho same value, diff data type
console.log(1 !== true); //true
console.log('juan' !== 'juan'); //false
console.log(juan !== 'juan'); //false; let juan = 'juan';

// RELATIONAL OPERATOR
// Returns a boolean value
let j = 50;
let k = 65;

// GT/Greater than operator ( > )
let isGreaterThan = j > k;
console.log(isGreaterThan); //false

// LT/Less than operator ( < )
let isLessThan = j < k;
console.log(isLessThan); //true

// GTE / Greater than or Equal operator (>=)
let isGTorEqual = j >= k;
console.log(isGTorEqual); //false

// LTE / Less than or Equal operator(<=)
let isLTorEqual = j <= k;
console.log(isLTorEqual); //true

let numStr = "30";
// forced coercion to change the string to a number
console.log(j > numStr); //true; forced coercion, sapilitang icoconvert

let str = "thirty";
console.log(j > str); //false; pag iforce coerce/convert and "thirty" to number, it will result to NaN or Not a Number
// (j(50) is greater than NaN or Not a Number)

// LOGICAL OPERATOR
let isLegalAge = true;
let isRegistered = false;

// Logical AND Operator (&& - Double Ampersand)
// Returns true if all operands are true
// true && true = true
// true && false = false

let allRequirementsMet = isLegalAge && isRegistered;
console.log(allRequirementsMet);

// Logical OR operator (|| - Double Pipe)
let someRequirementsMet = isLegalAge || isRegistered;
console.log(someRequirementsMet); // 













